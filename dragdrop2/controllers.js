
var App = angular.module('drag-and-drop', ['ngDragDrop','ui.bootstrap','ui.bootstrap.collapse', 'ui.bootstrap.dialog']);

App.factory('mySharedService', function($rootScope) {
	var sharedService = {};
	
	sharedService.broadcastItem = function( item ) {
		$rootScope.$broadcast( item );
	}
	
	return sharedService;
});

App.controller('projectController', function($scope, mySharedService, $http, $dialog) {
	$scope.steps = []; //[{kind:'say', title:'Say', voice:'man', language:'bf', loop:1}];
	
	$scope.languages = [{name:'en',text:'English'},{name:'fr',text:'French'},{name:'it',text:'Italian'},{name:'sp',text:'Spanish'},{name:'el',text:'Greek'}];
	$scope.methods = ['POST', 'GET'];
	
	// removes a step from a two level step hierarchy. The step array should always be called 'steps'
	$scope.removeStep = function (step) {
		var pos = $scope.steps.indexOf(step);
		if ( pos == -1 ) {
			for (var i = 0; i < $scope.steps.length; i ++) {
				var anystep = $scope.steps[i];
				if (anystep.steps != undefined ) {
					pos = anystep.steps.indexOf(step);
					if ( pos != -1 ) {
						anystep.steps.splice(pos,1);
						break;
					}
				}
			}
		}
		else
			$scope.steps.splice(pos,1);
			
		if ( pos == -1 )
				console.log( 'Could not remove step. Step not found' );
	}
	
	$scope.expandAll = function() {
		$.each($scope.steps, function (index, step) {
			step.isCollapsed = false;
		});
	}
	
	
	$scope.collapseAll = function() {
		$.each($scope.steps, function (index, step) {
			step.isCollapsed = true;
		});		
	}
	
	
	$scope.$on('generate', function () {
					
		$http({url: 'generate.php',
			method: "POST",
			data: $scope.steps/*{a:JSON.stringify($scope.steps)}*/,
			headers: {'Content-Type': 'application/data'}
		})
		.success(function (data, status, headers, config) {
            console.log('succesfully posted steps');
            $("#dialog .content").html(data);
			$( "#dialog" ).dialog();

        }).error(function (data, status, headers, config) {
            console.log('ERROR posting steps');
        });
        
	});
	
	$scope.$on('clear_steps', function () {
		$scope.steps = [];
	});
	
});

App.controller('verbController', function($scope, mySharedService) {
	$scope.verbs = [
					{kind:'say', label:'say', title:'Say something', phrase:'', voice:'man', language:'bf', loop:1, isCollapsed:false},
					{kind:'play', label:'play', title:'Play audio file', fileurl:'', loop:1, isCollapsed:false},
					{kind:'gather', label:'gather', title:'Gather digits', action:'', method:'POST', timeout:'', finishOnKey:'#', numDigits:'', steps:[], isCollapsed:false},
					{kind:'dial', label:'dial', title:'Dial', numberToCall:'', action:'', method:'POST', timeout:30, timeLimit:14400, callerId:'', steps:[], isCollapsed:false},
					{kind:'number', label:'number', title:'Number', numberToCall:'', sendDigits:'', numberUrl:''},
	];
	
	$scope.generate = function () {
			mySharedService.broadcastItem('generate');
	};
	
	$scope.clear_steps = function () {
			mySharedService.broadcastItem('clear_steps');
	};

	
	
});
