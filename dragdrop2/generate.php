<?php

$postdata = file_get_contents("php://input");
$steps = json_decode($postdata);


$element = new SimpleXMLElement("<Response></Response>");

	//print_r( $request );



function process_number_step( $parent_element, $step )
{
	$element = $parent_element -> addChild( $step->kind, $step->numberToCall );
	if ( $step->sendDigits)
		$element->addAttribute('sendDigits', $step->sendDigits);
	if ( $step->numberUrl)
		$element->addAttribute('numberUrl', $step->numberUrl);
		
	return $element;
}

function process_say_step( $parent_element, $step )
{
	$element = $parent_element -> addChild( $step->kind, $step->phrase );
	if ( $step->voice)
		$element->addAttribute('voice', $step->voice);
	if ( $step->language)
		$element->addAttribute('language', $step->language);
	if ( $step->loop)
		$element->addAttribute('loop', $step->loop);
		
	return $element;
}

function process_play_step( $parent_element, $step )
{
	$element = $parent_element -> addChild( $step->kind, $step->fileurl );
	if ( $step->loop)
		$element->addAttribute('loop', $step->loop);	
		
	return $element;
}

function process_gather_step( $parent_element, $step )
{
	$element = $parent_element -> addChild( $step->kind );
	if ( $step->method)
		$element->addAttribute('method', $step->method);
	if ( $step->action)
		$element->addAttribute('action', $step->action);		
	if ( $step->timeout)
		$element->addAttribute('timeout', $step->timeout);
	if ( $step->finishOnKey)
		$element->addAttribute('finishOnKey', $step->finishOnKey);
		
	if ( $step->steps )
		foreach ($step->steps as $child_step)
		{
			if ( $child_step->kind == 'say' )
				process_say_step( $element, $child_step );
			else
			if ( $child_step->kind == 'play' )
				process_play_step( $element, $child_step );	
				
			// ignore other step kids		
		}
					
	return $element;
}

function process_dial_step( $parent_element, $step )
{
	$element = $parent_element -> addChild( $step->kind, $step->numberToCall );
	if ( $step->method)
		$element->addAttribute('method', $step->method);
	if ( $step->action)
		$element->addAttribute('action', $step->action);		
	if ( $step->timeout)
		$element->addAttribute('timeout', $step->timeout);
	if ( $step->timeLimit)
		$element->addAttribute('timeLimit', $step->timeLimit);
	if ( $step->callerId)
		$element->addAttribute('callerId', $step->callerId);

		
	if ( $step->steps )
		foreach ($step->steps as $child_step)
		{
			if ( $child_step->kind == 'number' )
				process_number_step( $element, $child_step );
				
			// ignore other step kids		
		}
					
	return $element;
}


foreach ( $steps as $step )
{
	if ( $step -> kind == 'play' )
		process_play_step( $element, $step );
	else 
	if ( $step -> kind == 'say' )
		process_say_step( $element, $step );
	else
	if ( $step -> kind == 'gather' )
		process_gather_step( $element, $step );	
	else
	if ( $step -> kind == 'dial' )
		process_dial_step( $element, $step );		
}


echo (htmlentities($element->asXML()));


?>
